package com.crazymaker.l2cache.cluster.level2.canal;

import java.util.List;
import java.util.Map;

public interface DataLoader {

    String getRegion(String database, String table);
    String getKey(String database, String table, Map<String, String> row);
    String[] getKeys(String database, String table, List<Map<String, String>> rows);
    String[] getNoHotKeys(String database, String table, List<Map<String, String>> rows);

    String getCacheData(String database, String table, Map<String, String> e);

    String getCacheData(String region, String id);
}

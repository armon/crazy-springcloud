/**
 * Copyright (c) 2015-2017, Winter Lau (javayou@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.crazymaker.l2cache.cluster.level3;

import com.crazymaker.l2cache.config.J2CacheCoreConfig;
import com.crazymaker.l2cache.manager.CacheException;
import com.crazymaker.l2cache.manager.CacheProviderHolder;

import java.util.Properties;

/**
 * 集群策略工厂
 *
 * @author Winter Lau(javayou@gmail.com)
 */
public class L3PolicyFactory {

    private L3PolicyFactory() {
    }

    /**
     * 初始化集群消息通知机制
     *
     * @param holder    CacheProviderHolder instance
     * @param broadcast j2cache.broadcast value
     * @param props     broadcast configuations
     * @return ClusterPolicy instance
     */
    public final static L3Policy init(CacheProviderHolder holder, String broadcast, J2CacheCoreConfig props) {

        L3Policy policy = null;
        if ("okHttp".equalsIgnoreCase(broadcast)) {
            policy = L3PolicyFactory.okHttp(props, holder);
        } else if ("none".equalsIgnoreCase(broadcast)) {
            policy = new NoneL3Policy();
        } else {
            policy = L3PolicyFactory.okHttp(props, holder);
        }
        return policy;
    }


    private final static L3Policy okHttp(J2CacheCoreConfig props, CacheProviderHolder holder) {
        OkHttpL3Policy policy = new OkHttpL3Policy(props);
        return policy;
    }

    /**
     * 加载自定义的集群通知策略
     *
     * @param classname
     * @param props
     * @return
     */
    private final static L3Policy custom(String classname, Properties props, CacheProviderHolder holder) {
        try {
            L3Policy policy = (L3Policy) Class.forName(classname).newInstance();
            return policy;
        } catch (Exception e) {
            throw new CacheException("Failed in load custom cluster policy. class = " + classname, e);
        }
    }

}

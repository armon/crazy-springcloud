package com.crazymaker.springcloud.delaydelete;


public abstract class OpCache{
	private final ICache cache;
 
    private final DelayTrigger<String> TRIGGER = new DelayTrigger<>(new IDelayHandler<String>() {
 
        @Override
        public void handle(String t) {
            if (cache.isRunning()) {
                cache.delete(t);
            }
        }
 
    });
    private DelayTrigger<Object> delayTrigger;

    public OpCache(ICache client) {
        this.cache = client;
    }
    
    public void close() {
        delayTrigger.destroy();
    }
    /**
     * 删除key value 键值对
     */
    public void deleteObject(String key) {
        cache.delete(key);

        //模拟 业务的删除操作

        // 延迟双删除
        TRIGGER.put(key);
    }
}
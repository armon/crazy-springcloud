package com.crazymaker.cloud.disruptor.demo.business.impl;

import com.lmax.disruptor.EventHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * 类似于消费者
 * disruptor会回调此处理器的方法
 */
@Slf4j
public class LongEventWorkHandler2 implements EventHandler<LongEvent> {

    @Override
    public void onEvent(LongEvent event, long sequence, boolean endOfBatch) throws Exception {
        log.info(" 消费者2 consume {} " ,event.getValue());
        Thread.sleep(10000);

    }
}